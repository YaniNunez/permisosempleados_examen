﻿using Permisos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Permisos.Services.Interfaces
{
   public interface IPermisoService
    {
        public IList<Permiso> List();
        public Task Add(Permiso permiso);
        public Task Delete(int id);
        public Task Edit(Permiso permiso);
    }
}
