﻿using Microsoft.EntityFrameworkCore;
using Permisos.Models;
using Permisos.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Permisos.Services
{
    public class PermisoService : IPermisoService
    {
        private readonly EmpresaContext _context;

        public PermisoService(EmpresaContext context)
        {
            _context = context;
        }

        public async Task Add(Permiso permiso)
        {

            if (!string.IsNullOrEmpty(permiso.ApellidosEmpleado) &&
                !string.IsNullOrEmpty(permiso.NombreEmpleado))
            {
                permiso.FechaPermiso = DateTime.Now;
                _context.Permiso.Add(permiso);
                await _context.SaveChangesAsync();
            }
            else
            {
                throw new ArgumentException("Falta completar el nombre o apellido.");
            }

        }
        public IList<Permiso> List()
        {
            return _context.Permiso.ToList();

        }
        public async Task Delete(int id)
        {
            var permiso = await _context.Permiso.FindAsync(id);
            _context.Permiso.Remove(permiso);
            await _context.SaveChangesAsync();
        }

        public async Task Edit(Permiso permiso)
        {

            if (!string.IsNullOrEmpty(permiso.ApellidosEmpleado) &&
               !string.IsNullOrEmpty(permiso.NombreEmpleado))
            {
                _context.Permiso.Update(permiso);
            await _context.SaveChangesAsync();
            }
            else
            {
                throw new ArgumentException("Falta completar el nombre o apellido.");
            }
        }
    }
}
