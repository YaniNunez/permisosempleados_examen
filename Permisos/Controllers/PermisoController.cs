﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Permisos.Models;
using Permisos.Services.Interfaces;

namespace Permisos.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PermisoController : ControllerBase
    {
       

        private readonly ILogger<PermisoController> _logger;
        private readonly IPermisoService _permisoService;

        public PermisoController(ILogger<PermisoController> logger,IPermisoService permisoService)
        {
            _logger = logger;
            _permisoService = permisoService;
        }

        [HttpGet]
        public  IList<Permiso> GetAllAsync()
        {
            try
            {
               return  _permisoService.List();
            }
            catch (Exception ex)
            {

                throw new ArgumentException("Se produjo un error, detalles: ", ex.Message);
            }
         
        }
        [HttpPost]
        public async Task Add(Permiso permiso)
        {
            try
            {
                await _permisoService.Add(permiso);
            }
            catch (Exception ex)
            {

                throw new ArgumentException("Se produjo un error, detalles: ", ex.Message);
            }
           
        }
        [HttpPut]
        public async Task Edit(Permiso permiso)
        {
            try
            {
                await _permisoService.Edit(permiso);
            }
            catch (Exception ex)
            {

                throw new ArgumentException("Se produjo un error, detalles: ", ex.Message);
            }

        }
        [HttpDelete]
        public async Task Delete(int id)
        {
            try
            {
                await _permisoService.Delete(id);
            }
            catch (Exception ex)
            {

                throw new ArgumentException("Se produjo un error, detalles: ", ex.Message);
            }

        }
    }
}
