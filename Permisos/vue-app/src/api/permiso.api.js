﻿// eslint-disable-next-line no-unused-vars
import axios from 'axios'
export default {
	listPermisosAll() {
		return axios({
			method: 'GET',
			url: 'https://localhost:44354/permiso',
		})
	},
	addPermiso(permiso) {			
		return axios({
			method: 'PUT',
			url: 'https://localhost:44354/permiso',
			data: permiso
		})
	},
	editPermiso(permiso) {
		return axios({
			method: 'POST',
			url: 'https://localhost:44354/permiso',
			data: permiso
		})
	},
	deletePermiso(id) {
		return axios({
			method: 'DELETE',
			url: 'https://localhost:44354/permiso',
			data: {id:id}
		})
	},
	
}