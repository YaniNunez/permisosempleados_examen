﻿using System;
using System.Collections.Generic;

namespace Permisos.Models
{
    public partial class Permiso
    {
        public int Id { get; set; }
        public string NombreEmpleado { get; set; }
        public string ApellidosEmpleado { get; set; }
        public int TipoPermisoId { get; set; }
        public DateTime FechaPermiso { get; set; }

        public virtual TipoPermiso TipoPermiso { get; set; }
    }
}
