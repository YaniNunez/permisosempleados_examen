﻿using System;
using System.Collections.Generic;

namespace Permisos.Models
{
    public partial class TipoPermiso
    {
        public TipoPermiso()
        {
            Permiso = new HashSet<Permiso>();
        }

        public int Id { get; set; }
        public string Descrpcion { get; set; }

        public virtual ICollection<Permiso> Permiso { get; set; }
    }
}
